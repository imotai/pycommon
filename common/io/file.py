#encoding:utf-8
'''
Created on 2014-1-26
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import os
import sys
import tempfile

from common.net.http import StatefullHttpService
from common.shell.command import ShellHelper


def multi_run_wrapper(args):
    return download_from_url(*args)
def download_from_url(url,full_file_name,force=True):
    try:
        http_service = StatefullHttpService(None)
        content,status = http_service.get(url)
        if os.path.exists(full_file_name):
            os.remove(full_file_name)
        if not status:
            return False,content
        fd = open(full_file_name,"wb")
        fd.write(content)
        fd.close()
        return True,full_file_name
    except Exception,ex:
        return False,str(ex)
class CopyException(Exception):
    pass
class FileProcessor(object):
    def __init__(self,shellhelper = None):
        self.shellhelper = shellhelper or ShellHelper()
        self.is_windows = sys.platform.startswith( "win" )
    def copy_tree(self,file_from,file_to):
        """
        copy_tree base on shell, if file_from is folder and  copy_children is True,
        chidren of file_from will be copied or file_from 
        """
        if not os.path.exists(file_from):
            raise CopyException("%s does not exist"%file_from)
        if not os.listdir(file_from):
            return 
        if self.is_windows:
            file_from = file_from.replace("/",os.sep) 
            file_to = file_to.replace("/",os.sep)
            if file_from.endswith(os.sep):
                file_from = file_from[:-1]
        elif os.path.isdir(file_from) and not file_from.endswith(os.sep) :
            file_from = file_from + os.sep
        cp_command =self._build_copy_cmd()
        if not self.is_windows:
            cp_command.append(file_from+"*")
        else:
            cp_command.append(file_from)
        cp_command.append(file_to)
        if self.is_windows:
            code,stdout,errout = self.shellhelper.run_with_retuncode(cp_command)
            if code != 0:
                error = (stdout or "") + (errout or "")
                raise CopyException(error)
        else:
            code,stdout,errout = self.shellhelper.run_with_retuncode([" ".join(cp_command)],useshell = True)
            if code != 0:
                error = (stdout or "") + (errout or "")
                raise CopyException(error)
    def _build_unix_copy(self,force=True):
        cp_command = ["cp"]
        if force :
            cp_command.append("-R")
        return cp_command
    def _build_win_copy(self):
        copt_cmd = ["xcopy","/E","/Y"]
        return copt_cmd
    def _build_copy_cmd(self):
        if self.is_windows:
            return self._build_win_copy()
        else:
            return self._build_unix_copy(force=True)
    def build_temp_dir(self):
        path = tempfile.mkdtemp()
        return path 
    def build_tree(self,file_tuple_list):
        """
        the full_file_name is the key of file_dict,and the url is the value 
        """
        result_list = []
        tmp_path = self.build_temp_dir()
        #try:
        dowload_jobs = []
        for file_tuple in file_tuple_list:
            full_file_name = file_tuple[1]
            if file_tuple[0]:
                if full_file_name.rfind('/') != -1:
                    file_folder_path = tmp_path+os.sep+full_file_name[:full_file_name.rfind('/')]
                    if not os.path.exists(file_folder_path):
                        os.makedirs(file_folder_path)
            # folder
            else:
                new_add_folder = tmp_path+os.sep+full_file_name
                if not os.path.exists(new_add_folder):
                    os.makedirs(new_add_folder)
                continue
            file_path = tmp_path+os.sep+full_file_name
            dowload_jobs.append(("http://"+file_tuple[0],file_path))
            result_list.append(download_from_url("http://"+file_tuple[0],file_path))
        #result_list = self.process_pool.map(multi_run_wrapper, dowload_jobs)
        for result in result_list:
            if not result[0]:
                return None
        #except Exception,ex:
            #return None
        return tmp_path
    def save_tmp_file(self,tmp_path,file_name,fd):
        if file_name.rfind('/') != -1:
            file_folder_path = tmp_path+os.sep+file_name[:file_name.rfind('/')]
            if not os.path.exists(file_folder_path):
                os.makedirs(file_folder_path)
        file_path = tmp_path+os.sep+file_name
        self.file_processor.handle_uploaded_file(fd,file_path)
    @classmethod
    def get_size(cls,path):
        if os.path.isdir(path):
            return 0
        return os.path.getsize(path)/1024
class FileUtil(object):
    @classmethod
    def get_size(cls,path):
        if os.path.isdir(path):
            return 0
        return os.path.getsize(path)/1024
        
        