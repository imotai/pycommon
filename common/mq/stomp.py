#encoding:utf-8
'''
Created on 2014-1-26
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import Queue
import logging
import socket
from threading import Thread
import threading
import time

from common.time.profile import Stopwatch

class BaseFrame:
    def frame_to_str(self):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )
    @classmethod
    def init_connect_header(cls,host,login=None,password=None,client_id=None):
        header = {}
        header['accept-version'] = '1.1,1.2'
        header['host'] = host
        if client_id:
            header['client-id']=client_id
        if login:
            header['login'] = login
        if password:
            header['passcode'] = password
        return header
    @classmethod
    def init_send_header(cls,destination,content_type,content_length=0):
        header = {}
        header['destination'] = destination
        header['content-type'] = content_type
        if content_length:
            header['content-length'] = content_length
        return header
    @classmethod
    def init_subscribe_header(cls,destination,subscribe_id,subscribe_name=None,ack='auto'):
        header = {}
        header['destination'] = destination
        header['id'] =subscribe_id
        if subscribe_name:
            header['activemq.subscriptionName'] = subscribe_name
        header['ack'] = ack
        return header
    @classmethod
    def init_unsubscribe_header(cls,client_id):
        header={}
        header['id'] = client_id
        return header
    @classmethod
    def init_ack_header(cls,client_id,transaction=None):
        header = {}
        header['id'] = client_id
        if transaction:
            header['transaction'] = transaction
        return header
    @classmethod
    def init_disconnect(cls,receipt):
        header={}
        header['receipt'] = receipt
        return header
class ConnectFrame(BaseFrame):
    def __init__(self,host,login=None,password=None,client_id=None):
        self.header = BaseFrame.init_connect_header(host,login,password,client_id)
    def frame_to_str(self):
        frame_content_list = []
        command = 'CONNECT'
        frame_content_list.append(command)
        frame_content_list.append('\n')
        for key in self.header.keys():
            frame_content_list.append(key)
            frame_content_list.append(':')
            frame_content_list.append(str(self.header[key]))
            frame_content_list.append('\n')
        frame_content_list.append('\n')
        frame_content_list.append('\x00')
        return ''.join(frame_content_list)
class SubscribeFrame(BaseFrame):
    def __init__(self,destination,subscribe_id,subscribe_name=None,ack='auto'):
        self.header = BaseFrame.init_subscribe_header(destination,subscribe_id,subscribe_name,ack)
    def frame_to_str(self):
        command ='SUBSCRIBE'
        frame_content_list = []
        frame_content_list.append(command)
        frame_content_list.append('\n')
        for key in self.header.keys():
            frame_content_list.append(key)
            frame_content_list.append(':')
            frame_content_list.append(str(self.header[key]))
            frame_content_list.append('\n')
        frame_content_list.append('\n')
        frame_content_list.append('\x00')
        return ''.join(frame_content_list)
class UnSubscibeFrame(BaseFrame):
    def __init__(self,client_id):
        self.header = BaseFrame.init_unsubscribe_header(client_id)
    def frame_to_str(self):
        command = 'UNSUBSCRIBE'
        frame_content_list = []
        frame_content_list.append(command)
        frame_content_list.append('\n')
        for key in self.header.keys():
            frame_content_list.append(key)
            frame_content_list.append(':')
            frame_content_list.append(str(self.header[key]))
            frame_content_list.append('\n')
        frame_content_list.append('\n')
        frame_content_list.append('\x00')
        return ''.join(frame_content_list)
class SendFrame(BaseFrame):
    def __init__(self,destination,body,content_type='text/palin'):
        content_length = 0
        if body:
            content_length = len(body)
        self.body = body
        self.header = BaseFrame.init_send_header(destination, content_type, content_length)
    def frame_to_str(self):
        command = 'SEND'
        frame_content_list = []
        frame_content_list.append(command)
        frame_content_list.append('\n')
        for key in self.header.keys():
            frame_content_list.append(key)
            frame_content_list.append(':')
            frame_content_list.append(str(self.header[key]))
            frame_content_list.append('\n')
        frame_content_list.append('\n')
        frame_content_list.append(self.body)
        frame_content_list.append('\x00')
        return ''.join(frame_content_list)
class DisconnectFrame(BaseFrame):
    def __init__(self,receipt):
        self.header = BaseFrame.init_disconnect(receipt)
    def frame_to_str(self):
        command = 'DISCONNECT'
        frame_content_list = []
        frame_content_list.append(command)
        frame_content_list.append('\n')
        for key in self.header.keys():
            frame_content_list.append(key)
            frame_content_list.append(':')
            frame_content_list.append(str(self.header[key]))
            frame_content_list.append('\n')
        frame_content_list.append('\n')
        frame_content_list.append('\x00')
        return ''.join(frame_content_list)
class MessageFrame(BaseFrame):
    """
        用于解析来自server 的frame 包含如下frame:
    1, CONNECTED 和服务器成功建立链接回复
    2，MESSAGE 服务器向客户端推送的消息
    3，ERROR 服务器返回的错误信息
    4，RECEIPT 关闭服务器链接时返回完全接受了客户端消息的回应
    """
    def __init__(self,message):
        parser = ActiveMQFrameParser(message)
        self.command,self.header,self.body =parser.parse() 
    def is_error(self):
        if self.command =='ERROR':
            return False
        else:
            return True
    def get_body(self):
        return self.body
    def get_command(self):
        return self.command
    def get_header(self):
        return self.header
class FrameParser(object):
    def __init__(self,message):
        self.message = message
    def validate_message(self):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )
    def parse(self):
        raise NotImplementedError( "abstract method -- subclass %s must override" % self.__class__ )
class ActiveMQFrameParser(FrameParser):
    def __init__(self,message):
        FrameParser.__init__(self, message)
    def validate_message(self):
        activemq_endflag = '\x00\n'
        other_endflag = '\x00'
        is_activemq = self.message.endswith(activemq_endflag)
        is_other = self.message.endswith(other_endflag)
        if is_activemq or is_other:
            self.message = self.message.replace(activemq_endflag,'')
            self.message = self.message.replace(other_endflag,'')
            return True
        return False
    def parse(self):
        if not self.validate_message():
            return '',{},''
        #找出第一处出现两个换行符的位置及 body起始位置
        body_index_start = self.message.find('\n\n')
        part1 = self.message[0:body_index_start].strip()
        part2 = self.message[body_index_start:]
        part1_list = part1.split('\n')
        command = ''
        header = {}
        for part in part1_list:
            if not part:
                continue
            if part.find(':')==-1:
                command = part
                continue
            #AactiveMQ session格式
            #session:ID:xxxxx:xx
            if part.startswith('session'):
                seperate = part.find(':')
                k = part[0:seperate]
                v = part[seperate+1:]
            else:
                kv = part.split(':')
                k = kv[0]
                v = kv[1]
            header[k] = v
        return command,header,part2[2:]

class MQSocket(object):

    MESSAGE_MAX_LENGTH = 4096*1024

    def __init__(self,sock=None,blocking=True):
        if sock is None:
            self.sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
            self.sock.setblocking(blocking)
        else:
            self.sock = sock
            self.sock.setblocking(blocking)
    
    def __break_into_chunks(self,message):
        if not message:
            return []
        chunks = []
        message_length = len(message)
        if message_length<=MQSocket.MESSAGE_MAX_LENGTH:
            chunks.append(message)
        else:
            chunk_size =1 + message_length/MQSocket.MESSAGE_MAX_LENGTH
            for i in range(chunk_size):
                start = i*MQSocket.MESSAGE_MAX_LENGTH
                end = start + MQSocket.MESSAGE_MAX_LENGTH
                if end >message_length:
                    end = message_length
                chunks.append(message[start:end])
        return chunks
    def connect(self,host,port):
        self.sock.connect((host,port))

    def send_message(self,frame):
        message = frame.frame_to_str()
        if not message:
            return
        chunks = self.__break_into_chunks(message)
        for chunk in chunks:
            sended_length = self.sock.send(chunk)
            if sended_length == 0:
                raise RuntimeError("socket connection broken")
    def receive_message(self,length):
        message = self.sock.recv(length)
        if not message:
            raise RuntimeError("socket connection broken")
        return message
    def close(self):
        self.sock.close()
class Worker(Thread):
    def __init__(self,tasks,name,logger=None):
        Thread.__init__(self)
        self.tasks = tasks
        self.name = name
        self.setName(name)
        #when main thread exits,the thread will auto-exit
        self.setDaemon(True)
        self.logger = logger or logging.getLogger(__name__)
        self.start()
    def run(self):
        while True:
            job= self.tasks.get()
            try:
                stopwatch = Stopwatch()
                stopwatch.start()
                job.execute()
                stopwatch.stop()
                self.logger.info('the time which job consumed is %s'%stopwatch.pretty_formate())
            except Exception,ex:
                self.logger.error('worker[%s],error[%s]'%(self.name,str(ex)))
class WorkerPool(object):
    def __init__(self,pool_size):
        self.tasks = Queue(pool_size)
        for index in range(pool_size):
            Worker(self.tasks,'worker'+str(index))
    def add_task(self,job):
        self.tasks.put(job)

class Job(object):
    def __init__(self,func,*args,**kargs):
        self.func = func
        self.args = args
        self.kargs = kargs
    def execute(self):
        self.func(*self.args,**self.kargs)
class MQClient(threading.Thread):
    def __init__(self,host,port,user,password,client_id=None,logger=None):
        threading.Thread.__init__(self)
        self.logger = logger or logging.getLogger(__name__)
        self.host = host 
        self.user = user
        self.password = password
        self.port =port
        self.mqscoket = MQSocket()
        self.connected=False
        self.running = False
        if client_id:
            self.client_id =client_id
        self.setName("mqclient")
    def __init_connect(self):
        self.logger.info('INIT SOCKET CONNECT ,host->%s,port->%s'%(self.host,str(self.port)))
        self.mqscoket.connect(self.host, self.port)
        if self.client_id:
            connect_frame = ConnectFrame(self.host,self.user,self.password,self.client_id)
        else:
            connect_frame = ConnectFrame(self.host,self.user,self.password)
        self.mqscoket.send_message(connect_frame)
        data=self.mqscoket.receive_message(MQSocket.MESSAGE_MAX_LENGTH)
        message_frame = MessageFrame(data)
        if message_frame.get_command()=='CONNECTED':
            self.logger.info('CONNECT TO ACTIVEMQ SUCCESSFULLY ')
            self.connected = True
            self.running = True
        else:
            self.logger.error(data)
    def __init__disconnect(self):
        self.logger.info('START TO DISCONNECT')
        disconnect_frame = DisconnectFrame(9527)
        self.mqscoket.send_message(disconnect_frame)
        #等待server回复已处理完发送的消息
        data = self.mqscoket.receive_message(MQSocket.MESSAGE_MAX_LENGTH)
        message_frame = MessageFrame(data)
        if (message_frame.get_header().has_key("receipt-id") 
            and message_frame.get_header()['receipt-id']=='9527'):
            self.logger.info('DISCONNECT SUCCESSFULLY')
        else:
            self.logger.error('FAIL TO DISCONNECT CONNECTIONG')
    def subscribe(self,destination,subscribe_id,pool,call_back,subscribe_name=None):
        self.destination = destination
        self.pool = pool
        self.call_back = call_back
        self.subscribe_name =subscribe_name
        self.subscribe_id = subscribe_id
        def should_run_func():
            subscribe_frame = SubscribeFrame(self.destination,self.subscribe_id,subscribe_name=self.subscribe_name)
            self.mqscoket.send_message(subscribe_frame)
            data_buffer = []
            while self.running:
                try:
                    data = self.mqscoket.receive_message(1)
                    data_buffer.append(data)
                    if not data.endswith('\x00'):
                        continue
                    message_frame = MessageFrame(''.join(data_buffer))
                    data_buffer = []
                    self.logger.info(message_frame.get_command())
                    if message_frame.get_command()!='ERROR':
                        job = Job(self.call_back,message_frame.get_body())
                        self.pool.add_task(job)
                    else:
                        self.logger.error(data)
                        self.connected = False
                        self.running = False
                except Exception,ex:
                    self.logger.error('runtime error %s'%str(ex))
                    self.connected = False
                    self.running = False
        self.should_run_func = should_run_func
    def send(self,body,destination):
        try:
            send_frame = SendFrame(destination,body)
            self.mqscoket.send_message(send_frame)
        except RuntimeError:
            self.connected = False
            self.running = False
    def close_connection(self):
        self.running = False
        time.sleep(2)
        self.__init__disconnect()
        self.mqscoket.close()
        self.connected = False
    def run(self):
        try:
            if self.mqscoket:
                self.mqscoket.close()
            self.mqscoket = MQSocket()
            self.__init_connect()
            self.should_run_func()
        except Exception,ex:
            self.logger.error('error happens %s'%str(ex))
            pass
    def check_status(self):
        return self.connected
    def change_host(self,host,port):
        self.host = host
        self.port = port
    def retart(self):
        self.logger.info('restarting mqclient')
        self.run()
    def get_host(self):
        return self.host