#encoding:utf-8
'''
Created on 2014-1-26
@author: wangtaize@baidu.com
@copyright: www.baidu.com
用于监控服务是否正常运行，
以及自动重连机制,持有一个对象引用
对象需要暴露重连接口（需要是非阻塞），检查状态，和是否busy接口
以及设置主机，端口接口
'''
import logging
import threading
import time
class Keeper(threading.Thread):
    def __init__(self,target,
                      heart_beat_api=None,
                      logger=None,
                      retry_times=100,
                      name="keeper"):
        #TODO,implement heart_beat callback
        threading.Thread.__init__(self)
        self.logger = logger or logging.getLogger(__name__)
        self.target = target
        self.running = True
        self.setName(name)
        self.retry_times=retry_times
        self.setDaemon(False)
    def monitoring(self):
        count=0
        while self.running:
            time.sleep(5)
            if self.target.check_status():
                continue
            self.target.restart()
            #forever retry
            if self.retry_times==-1:
                continue
            if count<self.retry_times:
                count+=1
                continue
            self.running = False
            self.logger.error('sorry ,no host is avilable,we will exit the process')
    def close(self):
        self.running = False
    def run(self):
        self.logger.info('start monitor');
        self.monitoring()
    
        
