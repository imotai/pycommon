# -* encoding:utf-8 -*-
'''
Created on 2014-1-27
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import difflib
import os
import re

from common.shell.command import ShellHelper


class ParseDiffException(Exception):
    pass
class GitService(object):
    def __init__(self,shellhelper = None):
        self.shellhelper = shellhelper or ShellHelper()
        self.hashes = {}
        self.renames = {}
        self.validate_status = ['A','M','D']
    def is_in_workspace(self):
        check_command = ["git", "rev-parse","--is-inside-work-tree"]
        code,_,_ = self.shellhelper.run_with_retuncode( check_command )
        if code == 0:
            return True
        return False
    def get_unknows_list(self):
        unknows_list_command = ["git", "ls-files", "--exclude-standard", "--others"]
        code,output,_ = self.shellhelper.run_with_retuncode( unknows_list_command )
        if code != 0:
            return []
        lines = output.splitlines(True)
        return lines
    def get_base_content(self,file_hash,place_holder = False):
        get_base_content_command = ["git", "show", file_hash]
        code,output,_ = self.shellhelper.run_with_retuncode( get_base_content_command )
        if code != 0:
            return None
        return output
    def is_binary(self,file_name):
        raise NotImplementedError("not implemented")
    def get_change_list(self):
        st_command = ['git','status','-s']
        code,output,_ = self.shellhelper.run_with_retuncode( st_command )
        if code != 0:
            return []
        filname_list = output.splitlines()
        parsered_filename_list = []
        for filename in filname_list:
            if not filename:
                continue
            #choose the first char
            status = filename[0:1]
            if status not in self.validate_status:
                continue
            real_filename = filename[3:]
            parsered_filename_list.append((status,real_filename))
        return parsered_filename_list
    def get_head_content(self,file_name):
        get_head_command = ['git','show','HEAD:%s'%file_name]
        code,output,error = self.shellhelper.run_with_retuncode( get_head_command )
        if code != 0:
            raise Exception(error)
        return output
    def get_stage_content(self,file_name):
        get_stage_command = ['git','show',':%s'%file_name]
        code,output,error = self.shellhelper.run_with_retuncode( get_stage_command )
        if code != 0:
            raise Exception(error)
        return output
    def get_rep_name(self):
        rep_name_command = ['git','rev-parse','--show-toplevel']
        code,output,error = self.shellhelper.run_with_retuncode( rep_name_command )
        if code != 0:
            raise Exception(error)
        return output.split("/")[-1]
    def generate_diff(self,extra_args = []):
        env = os.environ.copy()
        # --no-ext-diff is broken in some versions of Git, so try to work around
        # this by overriding the environment (but there is still a problem if the
        # git config key "diff.external" is used).
        if 'GIT_EXTERNAL_DIFF' in env: del env['GIT_EXTERNAL_DIFF']
        diff_command = ["git", "diff", "--no-ext-diff", "--full-index", "-M"]
        if extra_args:
            diff_command.extend(extra_args)
        code,output,_ = self.shellhelper.run_with_retuncode(diff_command)
        if code != 0:
            return None
        return output
    def get_content_by_hash(self,file_hash):
        show_hash_command = ['git','show',file_hash]
        code,output,error = self.shellhelper.run_with_retuncode( show_hash_command )
        if code != 0:
            raise Exception(error)
        return output

class DiffProcessor(object):
    def __init__(self):
        self.NULL_HASH = "0"*40
        self.CHUNK_RE = re.compile( r"""
  @@
  \s+
  -
  (?: (\d+) (?: , (\d+) )?)
  \s+
  \+
  (?: (\d+) (?: , (\d+) )?)
  \s+
  @@
""", re.VERBOSE )
    def process(self,diff):
        file_name = None
        lines = diff.splitlines(True)
        rename_dict = {}
        hash_dict = {}
        for line in lines:
            filename_matcher = re.match( r"diff --git a/(.*) b/(.*)$", line )
            if filename_matcher:
                file_name = filename_matcher.group( 2 )
                if filename_matcher.group( 1 ) != filename_matcher.group( 2 ):
                    rename_dict[filename_matcher.group( 2 )] = filename_matcher.group( 1 )
            else:
                hash_matcher = re.match( r"index (\w+)\.\.(\w+)", line )
                if hash_matcher:
                    before, after = ( hash_matcher.group( 1 ), hash_matcher.group( 2 ) )
                    if before == self.NULL_HASH:
                        before = None
                    if after == self.NULL_HASH:
                        after = None
                    status = self._caculate_status(before, after)
                    hash_dict[file_name] = (status, before, after )
        return hash_dict ,rename_dict
    def split_to_patch(self,diff_data):
        lines = diff_data.splitlines( True )
        patch_dict = {}
        filename = None
        temp_diff_lines = []
        file_set = set()
        for line in lines:
            new_filename = None
            filename_matcher = re.match( r"diff --git a/(.*) b/(.*)$", line )
            if filename_matcher:
                new_filename = filename_matcher.group( 2 )
            if new_filename:
                if filename and temp_diff_lines and filename not in file_set :
                    temp_diff_lines = self._filter_last_crlf(temp_diff_lines)
                    patch_dict[filename] =  ''.join( temp_diff_lines ) 
                    file_set.add(filename)
                filename = new_filename
                temp_diff_lines = [line]
                continue
            if temp_diff_lines is not None:
                temp_diff_lines.append( line )
        if filename and temp_diff_lines \
           and filename not in file_set:
            temp_diff_lines = self._filter_last_crlf(temp_diff_lines)
            patch_dict[filename] =  ''.join( temp_diff_lines ) 
            file_set.add(filename)
        return patch_dict
    def parse_to_chunks(self,patch_diff):
        lines = patch_diff.splitlines(True)
        lineno = 0
        raw_chunk = []
        chunks = []
        old_range = new_range = None
        old_last = new_last = 0
        in_prelude = True
        for line in lines:
            lineno += 1
            if in_prelude:
                # Skip leading lines until after we've seen one starting with '+++'
                if line.startswith( "+++" ):
                    in_prelude = False
                continue
            match = self.CHUNK_RE.match( line )
            if match:
                if raw_chunk:
                    # Process the lines in the previous chunk
                    old_chunk = []
                    new_chunk = []
                    for tag, rest in raw_chunk:
                        if tag in ( " ", "-" ):
                            old_chunk.append( rest )
                        if tag in ( " ", "+" ):
                            new_chunk.append( rest )
                    # Check consistency
                    old_i, old_j = old_range
                    new_i, new_j = new_range
                    if len( old_chunk ) != old_j - old_i or len( new_chunk ) != new_j - new_i:
                        raise ParseDiffException("previous chunk has incorrect length")
                    chunks.append( ( old_range, new_range, old_chunk, new_chunk ) )
                    raw_chunk = []
                # Parse the @@ header
                old_ln, old_n, new_ln, new_n = match.groups()
                old_ln, old_n, new_ln, new_n = map( long,( old_ln, old_n or 1,new_ln, new_n or 1 ) )
                # Convert the numbers to list indices we can use
                if old_n == 0:
                    old_i = old_ln
                else:
                    old_i = old_ln - 1
                old_j = old_i + old_n
                old_range = old_i, old_j
                if new_n == 0:
                    new_i = new_ln
                else:
                    new_i = new_ln - 1
                new_j = new_i + new_n
                new_range = new_i, new_j
                # Check header consistency with previous header
                if old_i < old_last or new_i < new_last:
                    raise ParseDiffException("%s: chunk header out of order: %r"%( lineno, line))
                if old_i - old_last != new_i - new_last:
                    raise ParseDiffException("%s:%s: inconsistent chunk header: %r"%( lineno, line) )
                old_last = old_j
                new_last = new_j
            else:
                tag, rest = line[0], line[1:]
                if tag in ( " ", "-", "+" ):
                    raw_chunk.append( ( tag, rest ) )
                
                else:
                    raise ParseDiffException( "invaldate line %s"%line )
        if raw_chunk:
            # Process the lines in the last chunk
            old_chunk = []
            new_chunk = []
            for tag, rest in raw_chunk:
                if tag in ( " ", "-" ):
                    old_chunk.append( rest )
                if tag in ( " ", "+" ):
                    new_chunk.append( rest )
            # Check consistency
            old_i, old_j = old_range
            new_i, new_j = new_range
            if len( old_chunk ) != old_j - old_i or len( new_chunk ) != new_j - new_i:
                raise ParseDiffException( "%s: last chunk has incorrect length" %( lineno ))
            chunks.append( ( old_range, new_range, old_chunk, new_chunk ) )
            raw_chunk = []
        return chunks
    def patch_chunk(self,base_content, diff):
        old_lines = base_content.splitlines(True)
        chunks = self.parse_to_chunks(diff)
        if not chunks:
            return base_content
        new_content = []
        old_pos = 0
        for ( old_i, old_j ), ( _, _ ), old_chunk, new_chunk in chunks:
            eq = old_lines[old_pos:old_i]
            if eq:
                new_content.extend(eq) 
            old_pos = old_i
            # Check that the patch matches the target file
            if old_lines[old_i:old_j] != old_chunk:
                raise ParseDiffException("old trunk mismatch")
            # TODO(guido): ParsePatch knows the diff details, but throws the info away
            sm = difflib.SequenceMatcher( None, old_chunk, new_chunk )
            for _, _, _, j1, j2 in sm.get_opcodes():
                new_content.extend(new_chunk[j1:j2])
            old_pos = old_j
        # Copy the final matching chunk if any.str
        eq = old_lines[old_pos:]
        if eq:
            new_content.extend( eq )
        return "".join(new_content)
    def _filter_last_crlf(self,temp_diff_lines):
        last_line = temp_diff_lines[-1].replace('\n','').replace('\r\n','')
        if last_line == "\ No newline at end of file" :
            temp_diff_lines = temp_diff_lines[:-1]
            temp_diff_lines[-1] = temp_diff_lines[-1].replace('\n','').replace('\r\n','')
        return temp_diff_lines
    def _caculate_status(self,before ,after , is_rename = False):
        if before and after  and \
           before != after:
            return "M"+" "*7
        if before  and not after:
            return "D"+" "*7
        if not before  and after:
            if is_rename:
                return "A+"+" "*6
            return "A"+" "*7
        
    
    
            
        
    
    

    
    