'''
Created on 2014-1-26
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import os
import re

from common.shell.command import ShellHelper
import xml.etree.ElementTree as ET

TEXT_MIMETYPES = ['application/javascript', 'application/json',
                  'application/x-javascript', 'application/xml',
                  'application/x-freemind', 'application/x-sh',
                  'application/x-httpd-php',]
try:
    from cStringIO import StringIO as SI
except:
    from StringIO import StringIO as SI
class SubversionService(object):
    def __init__(self,shellhelper = None):
        #when the out is xml formate  
        self.STATUS_DICT = {"added":'A       ',"modified":"M       ","deleted":"D       "}
        self.shellhelper = shellhelper or ShellHelper()
        self.ls_cache = {}
    def checkout(self,svn_url):
        """
        co code from a sun_url
        """
        checkout_command = ["svn","co",svn_url]
        self.shellhelper.run_with_realtime_print(checkout_command,print_output = True)
    def get_last_version_from_log(self):
        '''get head revision through svn log.'''
        logcmd = ["svn", "log",'-l1','--xml']
        code,stdout,errorout = self.shellhelper.run_with_retuncode( logcmd )
        log_newest_rev = None
        if code != 0:
            return False,errorout
        root_element = ET.parse(SI(stdout)).getroot()
        try:
            log_newest_rev = root_element.find("logentry").attrib['revision']
            return True,log_newest_rev
        except:
            return False,None
    def add_file(self,file_path,sep = os.sep,force = True):
        last_sep_index = file_path.rfind(sep)
        if last_sep_index != -1:
            parent_folder = file_path[:last_sep_index]
            self.add_folder(parent_folder, recursive = False, sep = sep)
        self._add_to_control(file_path)
    def _add_to_control(self,target):
        add_command = ["svn","add","-N"]
        add_command.append(target)
        code,_,errorout = self.shellhelper.run_with_retuncode(add_command)
        if code !=0:
            return False,errorout
        return True,None 
    def is_under_control(self,target):
        svn_st_command = ["svn", "st", "-q","--xml","-N"]
        svn_st_command.append(target)
        code, output, errout = self.shellhelper.run_with_retuncode(svn_st_command)
        if code != 0:
            return False,errout
        under_controll = []
        try:
            root_element = ET.parse(SI(output)).getroot()
            entries = root_element.find('target').findall('entry')
            for entry in entries:
                status  = entry.find('wc-status').attrib['item']
                if status in self.STATUS_DICT:
                    under_controll.append((self.STATUS_DICT[status],entry.attrib['path']))
        except :
            pass
        if target in under_controll:
            return True,None
        return False,""
    def add_folder(self,folder,recursive = False,sep = os.sep):
        if recursive:
            add_folder_command = ["svn","add"]
            add_folder_command.append(folder)
            code,_,errorout= self.shellhelper.run_with_retuncode(add_folder_command)
            if code != 0:
                return False, errorout
        else:
            path_parts = folder.split(sep)
            for (index,_) in enumerate(path_parts):
                path = sep.join(path_parts[:index+1])
                status,_ = self.is_under_control(path)
                if status:
                    continue
                self._add_to_control(path)
        return True, None
    def rm_file(self,file_path_List,force = True):
        rm_command = ["svn","rm"]
        if force:
            rm_command.append("--force")
        rm_command.extend(file_path_List)
        code,_,errorout= self.shellhelper.run_with_retuncode(rm_command)
        if code !=0:
            return False,errorout
        return True,None
    def get_base_url(self):
        root_element = self._get_svn_info()
        if  root_element is None:
            return None
        base_url = root_element.find("entry").find("url")
        if  base_url is None :
            return None
        return base_url.text
    def get_changelist(self,extra_args = []):
        svn_st_command = ["svn", "st", "-q","--xml"]
        svn_st_command.extend(extra_args)
        code, output, errout = self.shellhelper.run_with_retuncode(svn_st_command)
        if code != 0:
            return False,errout
        try:
            root_element = ET.parse(SI(output)).getroot()
            targets = root_element.findall('target')
            changelist_list = root_element.findall("changelist")
            result = []
            for target in (targets + changelist_list):
                entries = target.findall('entry')
                for entry in entries:
                    if entry is None:
                        continue
                    status  = entry.find('wc-status').attrib['item']
                    if status in self.STATUS_DICT:
                        result.append((self.STATUS_DICT[status],entry.attrib['path']))
                    
            return True,result
        except Exception,ex:
            return False,str(ex)
    def get_unknows_list(self):
        svn_st_command = ["svn","st"]
        code, output, _ = self.shellhelper.run_with_retuncode(svn_st_command)
        if code != 0:
            return []
        lines = output.splitlines(True)
        unknown_list = []
        for line in lines:
            if not line.startswith("?"):
                continue
            unknown_list.append(line)
        return unknown_list
    def update(self,revision = None,force = False,extra_args = []):
        svn_up_command = ["svn", "up"]
        svn_up_command.extend(svn_up_command)
        if revision:
            svn_up_command.append("-r%s"%revision)
        if force:
            svn_up_command.append("--force")
        self.shellhelper.run_with_realtime_print(svn_up_command)
    def is_binary(self,file_path):
        is_binart_command = ["svn", "propget", "svn:mime-type", file_path]
        code, output, _ = self.shellhelper.run_with_retuncode(is_binart_command)
        if code:
            mimetype = ""
        else:
            mimetype = output.strip()
        if mimetype in TEXT_MIMETYPES:
            return False
        is_binary = bool( mimetype ) and not mimetype.startswith( "text/" )
        return is_binary
    def get_rep_root_path(self):
        if self.is_svn16():
            return self._find_svn16_root_path()
        root_element = self._get_svn_info()
        if  root_element is None:
            return None
        root_path = root_element.find("entry").find("wc-info").find("wcroot-abspath")
        if  root_path is None:
            return None
        return root_path.text.replace("/",os.sep)
    def get_co_revison(self):
        root_element = self._get_svn_info()
        entry = root_element.find("entry")
        if  entry is None or "revision" not in entry.attrib:
            return None
        return entry.attrib["revision"]
    def is_in_workspace(self):
        root_element = self._get_svn_info()
        if  root_element is None:
            return False
        return True
    def revert_local_change(self):
        _,change_list = self.get_changelist()
        target = []
        for change in change_list:
            path = change[1].split("/")[0]
            if path in target:
                continue
            target.append(change[1])
        revert_command = ["svn","revert","--depth=infinity"]
        revert_command.extend(target)
        code, _, _ = self.shellhelper.run_with_retuncode(revert_command)
        if code !=0:
            return False
        return True
    def revert(self,target):
        revert_command = ["svn","revert","--depth=infinity"]
        revert_command.append(target)
        code, _, _= self.shellhelper.run_with_retuncode(revert_command)
        if code !=0:
            return False
        return True
    def get_author(self):
        root_element = self._get_svn_info()
        author = root_element.find("entry").find("commit").find("author")
        if author is None:
            return None
        return author.text
    def is_svn16(self):
        code, output,_ = self.shellhelper.run_with_retuncode(["svn","--version"])
        if code != 0 :
            return False
        line = output.splitlines()[0]
        if line.find("1.6") != -1:
            return True
        return False
    def check_if_need_update(self):
        code, output,_ = self.shellhelper.run_with_retuncode(["svn", "status", "--ignore-externals",'-u'])
        if code != 0:
            return False
        lines = output.splitlines(True)
        file_need_to_update= []
        for line in lines:
            if line.find(' * ')!=-1:
                parts = line.split(' ')
                file_need_to_update.append(parts[-1])
        if file_need_to_update:
            return True
        return False
    def get_base_content(self,svn_url,revision = None):
        cat_command = ["svn","cat"]
        if revision:
            cat_command.append("%s@%s"%(svn_url,revision))
        else:
            cat_command.append(svn_url)
        code, output, error= self.shellhelper.run_with_retuncode(cat_command)
        if code != 0 :
            return False,error
        return True,output
    def get_file_list_from_diff( self, diff ):
        file_list = []
        for line in diff.splitlines( True ):
            if line.startswith( 'Index:' ) or line.startswith( 'Property changes on:' ):
                _, filename = line.split( ':', 1 )
                filename = filename.strip().replace( '\\', '/' )
                file_list.append(filename)
        return file_list
    
    def generate_diff_from_revision(self,start_revision='HEAD',end_revision=None):
        diff_command = ["svn",'diff']
        if start_revision and not end_revision:
            diff_command.append("--revision=%s"%start_revision)
        elif end_revision and end_revision:
            diff_command.append("--revision=%s:%s"%(start_revision,end_revision))
        code, output,_= self.shellhelper.run_with_retuncode(diff_command)
        if code != 0:
            return None
        return output
    def generate_diff_from_changelist(self,changelist):
        diff_command = ["svn",'diff','--changelist=%s'%changelist]
        code, output,_= self.shellhelper.run_with_retuncode(diff_command)
        if code != 0:
            return None
        return output
    def is_branch(self):
        base = self.get_base_url()
        if base.find('branches'):
            return True
        return False
    def get_first_revision_of_current_branch(self):
        code, output,_= self.shellhelper.run_with_retuncode(["svn","log","-l1","-r1:HEAD","--stop-on-copy","-q","--xml"])
        if code != 0:
            return None
        root_element = ET.parse(SI(output)).getroot()
        log_entry = root_element.find('logentry')
        if  log_entry is None or "revision" not in log_entry.attrib:
            return None
        return log_entry.attrib['revision']
    def need_update_files(self):
        code, output,_ = self.shellhelper.run_with_retuncode(["svn", "status", "--ignore-externals",'-u'])
        if code != 0:
            return []
        lines = output.splitlines(True)
        file_need_to_update= []
        for line in lines:
            if line.find(' * ')!=-1:
                parts = line.split(' ')
                file_need_to_update.append(parts[-1])
        return file_need_to_update
    def update_workcopy(self):
        self.shellhelper.run_with_realtime_print(['svn','up'])
    def _get_svn_info(self):
        code, output,_= self.shellhelper.run_with_retuncode(['svn','info',"--xml"])
        if code != 0 or not output :
            return None
        root_element = ET.parse(SI(output)).getroot()
        return root_element
    def _find_svn16_root_path(self):
        current_path = os.getcwd()
        while True:
            current_dot_svn =os.sep.join([current_path,".svn"])
            parent_dot_svn =os.sep.join([os.path.dirname(current_path),".svn"])
            if os.path.exists(current_dot_svn) and not os.path.exists(parent_dot_svn):
                return current_path
            new_current_path = os.path.dirname(current_path)
            if new_current_path == current_path:
                return None
            current_path = new_current_path
class DiffProcessor(object):
    def split_to_patch(self,diff_data):
        lines = diff_data.splitlines( True )
        patches = []
        filename = None
        temp_diff_lines = []
        file_set = set()
        for line in lines:
            new_filename = None
            if line.startswith( 'Index:' ):
                _, new_filename = line.split( ':', 1 )
                #windows
                new_filename = new_filename.strip().replace( '\\', '/' )
            elif line.startswith( 'Property changes on:' ):
                _, temp_filename = line.split( ':', 1 )
                temp_filename = temp_filename.strip().replace( '\\', '/' )
                if temp_filename != filename:
                    # File has property changes but no modifications, create a new diff.
                    new_filename = temp_filename
            if new_filename:
                if filename and temp_diff_lines \
                   and filename not in file_set :
                    patches.append( ( filename, ''.join( temp_diff_lines ) ) )
                    file_set.add(filename)
                filename = new_filename
                temp_diff_lines = [line]
                continue
            if temp_diff_lines is not None:
                temp_diff_lines.append( line )
        if filename and temp_diff_lines \
           and filename not in file_set:
            patches.append( ( filename, ''.join( temp_diff_lines ) ) )
            file_set.add(filename)
        return patches
    def parse_revision(self,patch_lines):
        base_revision = 0
        for line in patch_lines[:10]:
            if line.startswith( '@' ):
                break
            base_match = re.match( r'---\s.*\(.*\s(\d+)\)\s*$', line )
            if base_match and base_revision==0:
                base_revision = int( base_match.group( 1 ) )
                break
        return base_revision
    def is_binary(self, patch_lines):
        num_trunks = len([l for l in patch_lines if l.startswith('@@')])
        if num_trunks == 0:
            return True
        return False
    def caculate_patch_status_from_diff(self, patch_text):
        '''Get patch svn st, accord patch.
        '''
        patch_lines = patch_text.splitlines(True)
        num_added = len([l for l in patch_lines if l.startswith('+')]) - 1
        if self.is_binary(patch_lines):
            if num_added > 0:
                status = 'A       '
            else:
                status = 'M       '
            return status
        else:
            status = 'M       '
            rev = self.parse_revision(patch_lines)
            if rev is not None and rev == 0:
                status = 'A       '
                return status
            num_added = len([l for l in patch_lines if l.startswith('+')]) - 1
            num_equal = len([l for l in patch_lines if l.startswith(' ')])
            if num_equal > 0:
                status = 'M       '
            elif num_added == 0:
                status = 'D       '
            return status