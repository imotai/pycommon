'''
Created on 2014-1-26
@author: wangtaize@baidu.com
@copyright: www.baidu.com
'''
import logging
import os
import subprocess
import sys
USE_SHELL = sys.platform.startswith( "win" )
class ShellHelper(object):
    
    def __init__(self,logger = None):
        self.is_win = sys.platform.startswith( "win" )
        self.logger = logger or  logging.getLogger(__name__)
    def run_with_retuncode(self,command,
                                universal_newlines = True,
                                useshell = USE_SHELL,
                                env = os.environ):
        self.logger.info('run  with [%s] in dir[%s]'%(' '.join(command),os.getcwd()))
        try:
            p = subprocess.Popen( command,
                                  stdout = subprocess.PIPE,
                                  stderr = subprocess.PIPE,
                                  shell = useshell, 
                                  universal_newlines = universal_newlines,
                                  env = env )
            output = p.stdout.read()
            p.wait()
            errout = p.stderr.read()
            p.stdout.close()
            p.stderr.close()
            self.logger.info('end to run shell,return code is %s '%str(p.returncode))
            return p.returncode,output,errout
        except Exception,ex:
            self.logger.exception(ex)
            return -1,None,None
    def run_with_realtime_print(self,command,
                                universal_newlines = True,
                                useshell = USE_SHELL,
                                env = os.environ,
                                print_output = True):
        try:
            p = subprocess.Popen( command,
                                  stdout = subprocess.PIPE,
                                  stderr = subprocess.STDOUT,
                                  shell = useshell, 
                                  env = env )
            if print_output:
                for line in iter(p.stdout.readline,''):
                    sys.stdout.write(line)
                sys.stdout.write('\r')
            p.wait()
            return p.returncode
        except Exception,ex:
            self.logger.exception(ex)
            return -1
        