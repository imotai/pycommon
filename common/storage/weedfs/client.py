#! -*- coding:utf-8 -*-
'''
Created on 2013-9-29

@author: imotai
'''
import json

from common.net.http import StatefullHttpService, URLBuilder


class MasterUnavaliable(Exception):
    pass
class InvalidateParameter(Exception):
    pass
class AssignFailException(Exception):
    pass
class UploadFailException(Exception):
    pass
class WeedFSClient(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.base = URLBuilder.build_http(host, port)
        self.http_service = StatefullHttpService()
    def assign_fileid(self):
        """
                  请求文件id和上传文件服务url
        url不包含http 头,用url上传文件请求url格式为
        http://{url}/{fileid}
        """
        assign_url = "%s/dir/assign"%self.base
        result_str ,status= self.http_service.get(assign_url)
        if not status:
            raise AssignFailException("fail to assgn file id for %s"%result_str)
        result = json.loads(result_str)
        if "fid" in result and "url" in result:
            return result["fid"],result["url"]
        return None,None
    def check_status(self):
        status_url = "%s/dir/status"%self.base
        result_str ,status= self.http_service.get(status_url)
        if not status:
            raise MasterUnavaliable(result_str)
        result = json.loads(result_str)
        if "Topology" in result and \
            "DataCenters" in result["Topology"] and \
            len(result["Topology"]["DataCenters"])>0:
            return True
        return False
            
    def upload_with_fileid(self, fileid, 
                                 url,
                                 content,
                                 filename="unknown"):
        """
                    指定服务上传文件,采用http协议上传
        """
        filename = str(filename)
        if not url.startswith("http"):
            raise InvalidateParameter("this url should start with http but it's %s"%url)
        if not fileid:
            raise InvalidateParameter("this fileid should not be null but it's none")
        if url.endswith("/"):
            upload_url = url+fileid
        else:
            upload_url = url+"/"+fileid
        _,status = self.http_service.post_multipart(upload_url, [], [('file',filename,content)])
        if status:
            return upload_url
        return None
    def upload(self, content, filename="unknown"):
        """
                    返回如下格式数据
        {"fid":"3,01fbe0dc6f1f38",
        "fileName":"myphoto.jpg",
        "fileUrl":"localhost:8080/3,01fbe0dc6f1f38",
        "size":68231
        }
        """
        filename = str(filename)
        submit_url = "%s/submit"%self.base
        result_str,status = self.http_service.post_multipart(submit_url, [], [('file',filename,content)])
        if not status:
            raise UploadFailException(result_str)
        result = json.loads(result_str)
        return result 
    def get_file(self,url):
        url = str(url)
        content,status = self.http_service.get(url)
        if status:
            return content
        return None
