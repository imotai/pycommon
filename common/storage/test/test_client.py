'''
Created on 2013-10-17

@author: Wong
'''

from common.storage.weedfs.client import WeedFSClient
import common
import common


class Test(unittest.TestCase):


    def test_upload(self):
        test_content = os.path.abspath(os.path.join(os.path.dirname(__file__), u'test_client.py'))
        fd = open(test_content,"r")
        client = WeedFSClient('tc-iit-qa14.vm.baidu.com','8126')
        result = client.upload(fd.read(),  u'test_client.py')
        fd.close()
        self.assertEqual('test_client.py', result['fileName'])
        self.assertTrue(result['fileUrl'])
        self.assertTrue(result['fid'])
    def test_uploadwithfid(self):
        test_content = os.path.abspath(os.path.join(os.path.dirname(__file__), u'test_client.py'))
        fd = open(test_content,"r")
        client = WeedFSClient('tc-iit-qa14.vm.baidu.com','8126')
        fid ,url= client.assign_fileid()
        self.assertTrue(fid)
        self.assertTrue(url)
        url = "http://%s"%url
        upload_url = client.upload_with_fileid(fid, url, fd.read(), u'test_client.py')
        fd.close()
        self.assertEqual("%s/%s"%(url,fid), upload_url)
    def test_upload_gbkfile(self):
        test_content = os.path.abspath(os.path.join(os.path.dirname(__file__), u'gbk.file'))
        fd = open(test_content,"r")
        client = WeedFSClient('tc-iit-qa14.vm.baidu.com','8126')
        result = client.upload(fd.read(),  u'gbk.file')
        fd.close()
        self.assertEqual('gbk.file', result['fileName'])
        self.assertTrue(result['fileUrl'])
        self.assertTrue(result['fid'])
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()