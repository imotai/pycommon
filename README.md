###介绍
python常用工具类，这是我经常在项目中使用的工具方法，这里提炼出来，

1. 方便自己管理这些方法和bug修复
2. 分享希望大家能从中找到自己想要的方法

###依赖
只是对python版本有依赖，需要python>=2.7

###command模块
 *run_shell_with_returncode*
 
这个方法用于调用系统命令，支持Linux和window,阻塞,返回状态码，标准输出结果和标准错误输出结果

 *realtime_output_run_shell*
 
这个方法用于调用系统命令，支持Linux和window,阻塞,返回状态码,实时输出子进程输出

 *CommandCookies*
 
用于保存执行过的命令

 *get_cmd_path*
 
获得环境变量

###httputil模块

 *HttpService*

封装urllib提供get和post方法

###keeper模块

线程守护类
